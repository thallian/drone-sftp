FROM alpine:3.7

RUN apk --no-cache add \
    lftp \
    openssh

ADD /rootfs /
