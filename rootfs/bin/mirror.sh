#!/usr/bin/env sh

LFTP_PASSWORD="$SFTP_PASSWORD" lftp --env-password -u "$SFTP_USER" -p "$SFTP_PORT" -e "set sftp:auto-confirm yes; mirror --delete --reverse ${SFTP_SOURCE} ${SFTP_DEST}; bye" $"SFTP_HOST"
